<?php

declare(strict_types=1);

namespace Drupal\layout_builder_tomsaw\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Derives local tasks for entity types.
 */
class ManageDisplayTasks extends DeriverBase implements ContainerDeriverInterface {

  public function __construct(
    protected readonly EntityTypeManagerInterface $entityTypeManager,
    protected readonly EntityTypeBundleInfoInterface $entityBundleInfo
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $derivatives = [];

    # @fix Devel breaks Drupal\devel\Plugin\Derivative\DevelLocalTasks
    # -> Mandatory parameter is missing $entity_type_id
    if (\Drupal::moduleHandler()->moduleExists('devel'))
      return $derivatives;

    foreach ($this->entityTypeManager->getDefinitions() as $entity_type_id => $entity_type) {
      if ($entity_type->hasViewBuilderClass() && $entity_type->hasLinkTemplate('canonical')) {
        $task = [
          'route_name' => "entity.entity_view_display.$entity_type_id.default",
          'base_route' => "entity.$entity_type_id.canonical",
          'weight' => 100,
        ];

        if ($bundle_info = $this->entityBundleInfo->getBundleInfo($entity_type_id)) {
          foreach ($bundle_info as $bundle_id => $info) {
            $derivatives["entity.entity_view_display.$entity_type_id.$bundle_id"] = $task + [
              'title' => $info['label'] . ' Display',
              // @todo this doesn't work wrong. Find the proper way, how to provide the route-parameter
              'route_parameters' => [
                "{$entity_type_id}_type" => $bundle_id
              ] 
            ];
          }
        }
        else {
          $derivatives["entity.entity_view_display.$entity_type_id"] = $task + [
            'title' => 'Display'
          ];
        }
      }
    }
    
    return $derivatives;
  }

}
