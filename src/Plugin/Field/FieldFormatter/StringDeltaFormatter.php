<?php

namespace Drupal\layout_builder_tomsaw\Plugin\Field\FieldFormatter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\StringFormatter;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'string_delta' formatter.
 *
 * @FieldFormatter(
 *   id = "string_delta",
 *   label = @Translation("Plain text delta"),
 *   description = @Translation("Display specific delta of an multi string field."),
 *   field_types = {
 *     "string",
 *     "uri",
 *   }
 * )
 */
class StringDeltaFormatter extends StringFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'delta' => 0,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);

    $element['delta'] = [
      '#type' => 'number',
      '#title' => $this->t('Delta'),
      '#description' => $this->t('Delta of the item'),
      '#size' => 3,
      '#default_value' => $this->getSetting('delta'),
      '#required' => TRUE,
      '#weight' => -20,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $settings = $this->getSettings();
    $summary = parent::settingsSummary();
    $summary[] = $this->t("Delta: @delta", ['@delta' => $settings['delta']]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $entity = $items->getEntity();
    $entity_type = $entity->getEntityType();

    $delta = $this->getSetting('delta');
    if ($item = $items->get($delta)) {
      $view_value = $this->viewValue($item);

      if ($this->getSetting('link_to_entity') && !$entity->isNew() && $entity_type->hasLinkTemplate('canonical')) {
        return [[
          '#type' => 'link',
          '#title' => $view_value,
          '#url' => $this->getEntityUrl($entity),
        ]];
      }
      else {
        return [$view_value];
      }
    }
    else {
      return $this->t("Delta @delta does not exists", ['@delta' => $delta]);
    }
  }

}
