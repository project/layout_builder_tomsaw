<?php

namespace Drupal\layout_builder_tomsaw\EventSubscriber;

use Drupal\Core\Render\Element;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\layout_builder\LayoutBuilderEvents;
use Symfony\Component\HttpKernel\KernelEvents;
use Drupal\core_event_dispatcher\FormHookEvents;
use Symfony\Component\HttpKernel\Event\RequestEvent;

use Drupal\core_event_dispatcher\Event\Form\FormAlterEvent;
use Drupal\core_event_dispatcher\Event\Form\FormIdAlterEvent;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\layout_builder\Event\SectionComponentBuildRenderArrayEvent;

/**
 * Layout Builder TomSaw event subscriber.
 */
class LayoutBuilderTomsawSubscriber implements EventSubscriberInterface {
  public function __construct(
    private readonly RouteMatchInterface $currentRoute
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      FormHookEvents::FORM_ALTER => 'formAlter',
      'hook_event_dispatcher.form_layout_builder_add_block.alter' => 'addBlock',
      LayoutBuilderEvents::SECTION_COMPONENT_BUILD_RENDER_ARRAY => ['onBuildRender', 50],
      KernelEvents::REQUEST => ['onKernelRequest']
    ];
  }

  public function formAlter(FormAlterEvent $event): void {
    $form_object = $event->getFormState()->getFormObject();
    
    if($form_object instanceof \Drupal\layout_builder\Form\ConfigureBlockFormBase) {
      $form = &$event->getForm();
      
      // Remove pointless admin infos
      unset($form['settings']['admin_label']);

      // Close all children because we're using A LOT of ui_styles and scrolling is pain
      foreach(Element::children($form) as $children) {
        if(isset($form[$children]['#open'])) {
          $form[$children]['#open'] = FALSE;
        }
      }
    }
  }

  /**
   * Adjust some block_content default_values
   */
  public function addBlock(FormIdAlterEvent $event): void {
    $formatter = &$event->getForm()['settings']['formatter'];

    $formatter['label']['#default_value'] = "hidden";

    if(isset($formatter['type']['#options']['entity_reference_entity_view'])) {
      $formatter['type']['#default_value'] = 'entity_reference_entity_view';
    }
  }

  public function onBuildRender(SectionComponentBuildRenderArrayEvent $event): void {
    $component = $event->getComponent();
    $object_fit = $component->get('object_fit');
    if($object_fit) {
      $build = $event->getBuild();
      $build['#attributes']['class'][] = 'forward-height';
      $build['#attributes']['class'][] = 'of-' . $object_fit;
      $event->setBuild($build);
    }
  }

  /**
   * Kernel request event handler.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   Response event.
   */
  public function onKernelRequest(RequestEvent $event) {
    $routename = $this->currentRoute->getRouteName();

    if(str_starts_with($routename, 'layout_builder.')) {
      $event->getRequest()->request->set('dialogOptions', ['width' => 'min(90vw, 36rem)']);
    }
  }
}
