<?php

declare(strict_types=1);

namespace Drupal\layout_builder_tomsaw\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the segment entity edit forms.
 */
final class SegmentForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $result = parent::save($form, $form_state);

    $entity = $this->getEntity();

    $message_arguments = ['%title' => $entity->toLink()->toString()];
    $logger_arguments = [
      '%title' => $entity->label(),
      'link' => $entity->toLink($this->t('View'))->toString(),
    ];

    switch ($result) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('New segment %title has been created.', $message_arguments));
        $this->logger('layout_builder_tomsaw')->notice('Created new segment %title', $logger_arguments);
        break;

      case SAVED_UPDATED:
        $this->messenger()->addStatus($this->t('The segment %title has been updated.', $message_arguments));
        $this->logger('layout_builder_tomsaw')->notice('Updated segment %title.', $logger_arguments);
        break;
      
      default:
        throw new \LogicException('Could not save the entity.');
    }

    $form_state->setRedirectUrl($this->entity->toUrl());

    return $result;
  }

}
