<?php

declare(strict_types=1);

use Drupal\user\UserInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Form\FormStateInterface;
use Drupal\layout_builder_tomsaw\SegmentInterface;
use Drupal\layout_builder\Form\OverridesEntityForm;

function layout_builder_tomsaw_page_attachments(array &$variables): void {
  $variables['#attached']['library'][] = 'layout_builder_tomsaw/modern_font_stack';
  $variables['#attached']['library'][] = 'layout_builder_tomsaw/styles';

  // entity_block defines no library we could make depenend on 'layout_builder_tomsaw/entity_block'
  // As entity_block is small, just attach it to any page for now
  if(\Drupal::service('module_handler')->moduleExists('entity_block')) {
    $variables['#attached']['library'][] = 'layout_builder_tomsaw/entity_block';
  }
}

/**
 * Implements hook_library_info_alter.
 * Module way to extend libraries. Similar to 'libraries-extend' applicable in THEME.info.yml but only for themes
 */
function layout_builder_tomsaw_library_info_alter(array &$libraries, string $extension): void {
  switch ($extension) {
    case 'layout_builder':
      if (isset($libraries['drupal.layout_builder'])) {
        // Completely remove cores layout-builder.css ...
        unset($libraries['drupal.layout_builder']['css']['theme']['css/layout-builder.css']);

        // ... and attach our own library with different css files architecture
        $libraries['drupal.layout_builder']['dependencies'][] = 'layout_builder_tomsaw/layout-builder';

        // Absolute path to where the css is stored
        $css_components_path = '/' . \Drupal::service('extension.list.module')->getPath('layout_builder_tomsaw') . '/css/components';
        // $libraries['drupal.layout_builder']['css']['theme'][$css_path . '/layout-builder.css'] = [];
        $libraries['drupal.layout_builder']['css']['theme'][$css_components_path . '/ui-styles.css'] = [];
        $libraries['drupal.layout_builder']['css']['theme'][$css_components_path . '/off-canvas.css'] = [];


      }
      break;
    case 'layout_discovery':
      // unset all libraries
      $libraries = [];
      break;
    case 'media_library':
      if (isset($libraries['widget'])) {
        $libraries['widget']['dependencies'][] = 'layout_builder_tomsaw/media_library.widget';
      }
      break;
    case 'gin':
      if (isset($libraries['tabs'])) {
        $libraries['tabs']['dependencies'][] = 'layout_builder_tomsaw/gin.tabs';
      }
      break;
    case 'olivero':
      unset($libraries['layout_discovery_section']);

      if (isset($libraries['global-styling'])) {
        $libraries['global-styling']['dependencies'][] = 'layout_builder_tomsaw/olivero';

        // @todo as config checkbox
        foreach([
          'grid.css',
          'layout-content-narrow.css',
          'layout-content-narrow.css',
          'region-content.css',
          'region-hero.css',
          'region.css'
        ] as $filename) {
          unset($libraries['global-styling']['css']['layout']['css/layout/' . $filename]);
        }
      }
      break;
    case 'leaflet':
      $libraries['leaflet']['dependencies'][] = 'layout_builder_tomsaw/leaflet';
      break;
    case 'splide':
      $libraries['base']['dependencies'][] = 'layout_builder_tomsaw/splide';
      break;
    case 'easy_responsive_images':
      if (isset($libraries['resizer'])) {
        // @fixme browser does not request css/components/easy_responsive_images.css
        $libraries['reziser']['dependencies'][] = 'layout_builder_tomsaw/easy_responsive_images';
      }
      break;
  }
}

function layout_builder_tomsaw_form_alter(array &$form, FormStateInterface &$form_state) {

  $form_object = $form_state->getFormObject();

  if($form_object instanceof OverridesEntityForm) {
    // push form to the block-end
    $form['#weight'] = 10;
  }

  // Uses the same architecture like ui_styles
  // @see ui_styles_layout_builder.module
  if (in_array($form['#form_id'], ['layout_builder_add_block', 'layout_builder_update_block'])) {
    $component = $form_object->getCurrentComponent();

    $config = $component->get('configuration');
    if ($config['id'] == "entity_block:media") {
      $form['settings']['object_fit'] = [
        '#type' => 'select',
        '#title' => "Object Fit",
        '#empty_option' => "- Fill -",
        '#options' => [
          'cover' => 'Cover',
          'contain' => 'Contain',
          'scale-down' => 'Scale Down',
          'none' => 'None'
        ],
        '#default_value' => $component->get('object_fit')
      ];

      // Our submit handler must execute before the default one, because the
      // default handler stores the section & component data in the tempstore
      // and we need to update those objects before that happens.
      array_unshift($form['#submit'], '_layout_builder_tomsaw_block_plugin_submit_form');
    }
  }
}

function _layout_builder_tomsaw_block_plugin_submit_form(array $form, FormStateInterface $form_state): void {
  $form_object = $form_state->getFormObject();
  $component = $form_object->getCurrentComponent();

  $component->set('object_fit', $form_state->getValue('settings')['object_fit']);
}

function layout_builder_tomsaw_preprocess_html(array &$variables): void {
  if (($segment = \Drupal::routeMatch()->getParameter('segment')) /* || ($segment = \Drupal::routeMatch()->getParameter('segment_preview')) */) {
    if ($segment instanceof SegmentInterface) {
      $variables['attributes']['class'][] = 'entity-segment';
    }
  }
}

/**
 * Implements hook_theme().
 */
function layout_builder_tomsaw_theme(): array {
  return [
    'segment' => [
      'render element' => 'elements',
    ],
  ];
}

/**
 * Prepares variables for segment templates.
 *
 * Default template: segment.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the segment information and any
 *     fields attached to the entity.
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_segment(array &$variables): void {
  $variables['view_mode'] = $variables['elements']['#view_mode'];
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}

function layout_builder_tomsaw_preprocess_menu_local_tasks(array &$variables): void {
  $reorder = [
    // "entity.segment.collection" => -20, already set in layout_builder_tomsaw.links.task.yml
    "entity.block_content.collection" => -19,
    "entity.media.collection" => -18,
    "system.admin_content" => -17,
    // "entity.pos.collection" => 20,
    // "system.admin_content" => 0,
    // "views_view:view.simplenews_newsletters.page_1" => 0,
    "views_view:view.files.page_1" => 20,
  ];

  foreach ($reorder as $id => $weight) {
    if (isset($variables['primary'][$id])) {
      $variables['primary'][$id]['#weight'] = $weight;
    }
  }
}

/**
 * Implements hook_user_cancel().
 */
function layout_builder_tomsaw_user_cancel(array $edit, UserInterface $account, string $method): void {
  switch ($method) {
    case 'user_cancel_block_unpublish':
      // Unpublish segments.
      $storage = \Drupal::entityTypeManager()->getStorage('segment');
      $segment_ids = $storage->getQuery()
        ->condition('uid', $account->id())
        ->condition('status', 1)
        ->accessCheck(FALSE)
        ->execute();
      foreach ($storage->loadMultiple($segment_ids) as $segment) {
        $segment->set('status', FALSE)->save();
      }
      break;

    case 'user_cancel_reassign':
      // Anonymize segments.
      $storage = \Drupal::entityTypeManager()->getStorage('segment');
      $segment_ids = $storage->getQuery()
        ->condition('uid', $account->id())
        ->accessCheck(FALSE)
        ->execute();
      foreach ($storage->loadMultiple($segment_ids) as $segment) {
        $segment->setOwnerId(0)->save();
      }
      break;
  }
}

/**
 * Implements hook_ENTITY_TYPE_predelete() for user entities.
 */
function layout_builder_tomsaw_user_predelete(UserInterface $account): void {
  // Delete segments.
  $storage = \Drupal::entityTypeManager()->getStorage('segment');
  $segment_ids = $storage->getQuery()
    ->condition('uid', $account->id())
    ->accessCheck(FALSE)
    ->execute();
  $segments = $storage->loadMultiple($segment_ids);
  $storage->delete($segments);
  // Delete old revisions.
  $segment_ids = $storage->getQuery()
    ->allRevisions()
    ->condition('uid', $account->id())
    ->accessCheck(FALSE)
    ->execute();
  foreach (array_keys($segment_ids) as $revision_id) {
    $storage->deleteRevision($revision_id);
  }
}
