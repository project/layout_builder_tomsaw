/**
 * @file
 * tomsaw_layout_builder dropdown behaviors.
 */


(function ($, Drupal) {

	'use strict';
	/**
	 * Behavior description.
	 */
	Drupal.behaviors.dropdown = {
		attach: function (context, settings) {
			const dropdowns = context.querySelectorAll('.dropdown');

			dropdowns.forEach(function (dropdown) {
				const button = dropdown.querySelector('button');
				const content = dropdown.querySelector('.dropdown-content');
				Popper.createPopper(button, content, {
					placement: 'auto',
				});
			});
		}
	};

}(jQuery, Drupal));
