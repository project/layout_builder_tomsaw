<?php

declare(strict_types=1);

namespace Drupal\layout_builder_tomsaw;

use Drupal\ui_styles\StylePluginManager;
use Symfony\Component\Yaml\Yaml;

class GenericStylePluginManager extends StylePluginManager
{
  const domains = [
    'Section styles' => 'section',
    'Block styles' => 'block',
    'Title styles' => 'title',
    'Content styles' => 'content',
    'Exposed form' => 'exposed_form',
    'Style' => 'style',
  ];
  const scopes = [
    "layout_builder.configure_section" => 'section',
    "layout_builder.update_block" => 'block',
    "views_ui.form_display" => 'view'
  ];


  protected string $domain;
  public function getDefinitions(): array
  {
    $definitions = parent::getDefinitions();
    
    $route_name = \Drupal::request()->attributes->get('_route');
    if (isset($this->domain) && key_exists($route_name, self::scopes)) {
      $scope = self::scopes[$route_name] . ':' . $this->domain;

      // Filter out restricted styles
      $definitions = array_filter($definitions, function ($definition) use ($scope) {
        $additional = $definition->getAdditional();
        if (!isset($additional['restriction'])) {
          return TRUE;
        }

        foreach ($additional['restriction'] as $restriction) {
          if (str_starts_with($scope, $restriction)) {
            return TRUE;
          }
        }
        return FALSE;
      });
    }

    return $definitions;
  }

  /**
   * {@inheritdoc}
   */
  public function alterForm(array $form, array $selected = [], string $extra = '', string $theme = ''): array
  {
    // @todo MR solid code and remove this shabby hack:
    // Lookup title scope in forms #title
    // @see ui_styles_layout_builder.module ui_styles_layout_builder_form_alter()
    $title = (string) $form['#title'];

    if (isset(self::domains[$title]))
      $this->domain = self::domains[$title];
    // else
      // throw new \InvalidArgumentException("Title '$title' not covered.");


    $form = parent::alterForm($form, $selected, $extra, $theme);

    // A different topic but lets place it here for simplicity
    $form['#open'] = TRUE;

    return $form;
  }




  /* Support for generic options */
  protected array $reusable_options;

  private function getGenericOptions(string|array $selection): array
  {
    if (!isset($this->reusable_options)) {
      $filename = 'layout_builder_tomsaw.ui_styles.generic.yml';
      $file_path = $this->moduleHandler->getModule('layout_builder_tomsaw')->getPath() . '/' . $filename;

      if (file_exists($file_path))
        $this->reusable_options = Yaml::parseFile($file_path);
      else
        throw new \Exception($filename . ' not found.');
    };

    $selection = is_array($selection) ? $selection : [$selection];

    $options = [];
    foreach ($selection as $key)
      if (isset($this->reusable_options[$key]))
        $options += $this->reusable_options[$key];
      else
        throw new \Exception("Generic option '$key' not found.");
    return $options;
  }

  public static function keyToPrefix(string $key): string
  {
    return join('', array_map(fn($v) => $v[0], explode('_', $key)));
  }

  public function processDefinition(&$definition, $plugin_id): void
  {
    // Generate repetive options
    // @todo use a Drupal\Core\Plugin\Discovery\DiscoveryDecorator instead?
    // @see https://www.drupal.org/docs/drupal-apis/plugin-api/discovery-decorators
    if (isset($definition['options']['generic'])) {
      $prefix = self::keyToPrefix($definition['id']);
      $generic_options = $this->getGenericOptions($definition['options']['generic']);
      foreach ($generic_options as $key => $value)
        $definition['options']["{$prefix}-{$key}"] = $value;

      unset($definition['options']['generic']);
    }

    parent::processDefinition($definition, $plugin_id);
  }
}
