<?php

declare(strict_types=1);

namespace Drupal\layout_builder_tomsaw;

use Drupal\easy_responsive_images\EasyResponsiveImagesManager;

/**
 * Overwrites the initialImagesConfiguration method to use
 * image-styles: 'xxsmall', 'xxsmall', 'xsmall', 'small', 'medium', 'large', 'xlarge', 'xxlarge', 'xxxlarge'
 */
final class LayoutBuilderTomsawEasyResponsiveImagesManager extends EasyResponsiveImagesManager {
  /**
   * The initial image configuration array.
   *
   * @return array|null
   *   Returns the initial images configuration as an array.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function initialImagesConfiguration(): ?array {
    if ($this->configuration === NULL) {
      $this->configuration = [];
      $image_style_storage = $this->entityTypeManager->getStorage('image_style');
      $image_style_ids = $image_style_storage->getQuery()
        ->condition('name', ['xxsmall', 'xxsmall', 'xsmall', 'small', 'medium', 'large', 'xlarge', 'xxlarge', 'xxxlarge'], 'IN')
        ->execute();
      $loaded_styles = $image_style_storage->loadMultiple($image_style_ids);

      // we are dealing with a responsive image style.
      // [, $aspect_w, $aspect_h, $width] = $style_parts;
      // $group = $aspect_w . '_' . $aspect_h;

      // we are dealing with a scaling image.
      $group = 'scale';

      foreach ($loaded_styles as $style_name => $style) {
        $this->configuration[$group][$style_name]['style'] = $style;

        foreach($style->getEffects()->getConfiguration() as $style_config) {
          foreach(['width', 'height'] as $dim) {
            if(isset($style_config['data'][$dim])) {
              $this->configuration[$group][$style_name][$dim] = $style_config['data'][$dim];
            }
          }
        }
      }
    }
    return $this->configuration;
  }
}
