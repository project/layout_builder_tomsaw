<?php

declare(strict_types = 1);

namespace Drupal\layout_builder_tomsaw\EventSubscriber;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

final class LayoutBuilderTomsawRouteSubscriber extends RouteSubscriberBase {

  /**
   * Don't use admin routes when editing block_content so the correct twig and css is used
   * For the Layout task, a patch is required. @see patche/block_content_dont_use_admin_route.patch
   */
  protected function alterRoutes(RouteCollection $collection): void {
    // @see https://www.drupal.org/node/2187643
    foreach(['canonical', 'edit_form', 'delete_form'] as $task)
      if($route = $collection->get("entity.block_content.$task"))
        $route->setOption('_admin_route', false);
  }
}
