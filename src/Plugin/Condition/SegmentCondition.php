<?php declare(strict_types = 1);

namespace Drupal\layout_builder_tomsaw\Plugin\Condition;

use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\layout_builder_tomsaw\Entity\Segment;
/**
 * Provides a 'Segment Condition' condition.
 *
 * @Condition(
 *   id = "segment_condition",
 *   label = @Translation("Segements"),
 * )
 */
final class SegmentCondition extends ConditionPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Constructs a new SegmentCondition instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    private readonly EntityTypeManagerInterface $entityTypeManager,
    private readonly CurrentRouteMatch $currentRoute
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return ['segments' => []] + parent::defaultConfiguration();
  }

  private function getOptions() {
    $storage = $this->entityTypeManager->getStorage('segment');

    foreach($storage->loadMultiple() as $id => $segment) {
      $options[$id] = $segment->label();
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form['segments'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Segments'),
      '#options' => $this->getOptions(),
      '#default_value' => $this->configuration['segments'],
    ];
    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->configuration['segments'] = array_filter($form_state->getValue('segments'));
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function summary(): string {
    $storage = $this->entityTypeManager->getStorage('segment');
    $entities = $storage->loadMultiple($this->configuration['segments']);
    $labels = array_map(function($entity) { return $entity->label(); }, $entities);

    $message = $this->isNegated() ? "All Segments but @segments" : "Segments ";
    return (string) $this->t($message, [
        '@segments' => join(', ', $labels)
      ],
    );
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate(): bool {
    // @fixme using $this->isNegated() == true is evaluated falsy
    // Returns true if no segments are selected and negate option is disabled.
    if (empty($this->configuration['segments']) && !$this->isNegated()) {
      return TRUE;
    }

    // @workaround presence of section_storage suggests an edit form
    if($this->currentRoute->getParameter('section_storage'))
      return FALSE;

    if ($segment = $this->currentRoute->getParameter('segment')) {
      assert($segment instanceof Segment);
      return in_array($segment->id(), $this->configuration['segments']);
    }
    return FALSE;
  }

}
