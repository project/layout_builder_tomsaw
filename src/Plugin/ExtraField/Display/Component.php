<?php

namespace Drupal\layout_builder_tomsaw\Plugin\ExtraField\Display;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\commerce_tomsaw\Entity\ProductVariationEnhanced;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\extra_field_plus\Plugin\ExtraFieldPlusDisplayFormattedBase;

use Drupal\Core\Plugin\Component as CoreComponent;

/**
 * @ExtraFieldDisplay(
 *   id = "component",
 *   label = @Translation("Component"),
 *   description = @Translation("Render a single directory component."),
 *   bundles = {
 *     "*",
 *   }
 * )
 */
class Component extends ExtraFieldPlusDisplayFormattedBase
{
  use StringTranslationTrait;
  
  private static $component_id = 'commerce_rollgut:rollgut';
  private CoreComponent $component;

  public function getLabel()
  {
    return $this->component->metadata->name;
  }

  /**
   * {@inheritdoc}
   */
  protected static function extraFieldSettingsForm(): array
  {
    $form = parent::extraFieldSettingsForm();

    $component = \Drupal::service('plugin.manager.sdc')->getDefinition(self::$component_id);
    foreach($component['variants'] as $variant => $data)
      $options[$variant] = $data['title']; 

    $form['variants'] = [
      '#type' => 'checkboxes',
      '#title' => t('Variants'),
      '#options' => $options
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected static function settingsSummary(string $field_id, string $entity_type_id, string $bundle, string $view_mode = 'default'): array
  {
    $variants = array_filter(self::getExtraFieldSetting($field_id, 'variants', $entity_type_id, $bundle, $view_mode));
    
    if($variants)
      return [t("Variants: @variants", ['@variants' => implode(", ", $variants)])];
    else
      return [t("No variants selected")];
  }

  public function viewElements(ContentEntityInterface $entity)
  {
    assert($entity instanceof ProductVariationEnhanced);
    $props = $entity->getAttributeIds();

    foreach(array_filter($this->getEntityExtraFieldSetting('variants')) as $variant) {
      $build[] = [
        '#type' => 'component',
        '#component' => "commerce_rollgut:" . $entity->bundle(),
        '#props' => ['variant' => $variant] + $props,
      ];
    }

    return $build;
  }
}
